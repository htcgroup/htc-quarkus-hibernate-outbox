package com.gitlab.htcgroup.outbox;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.persistence.EntityManager;

@ApplicationScoped
public class DomainEventDispatcher {
    
    private static final Logger LOG = LoggerFactory.getLogger(DomainEventDispatcher.class);
    
    @Inject
    EntityManager entityManager;
    
    /**
     * An event handler for {@link DomainEvent} events and will be called when the event fires.
     *
     * @param event the domain event
     */
    public void onDomainEvent(@Observes DomainEvent event) {
        LOG.debug("A domain event was found for type {}", event.getType());
        
        final OutboxEvent outboxEvent = getDataFromEvent(event);
        
        Session session = entityManager.unwrap(Session.class);
        session.save(outboxEvent);
        session.setReadOnly(outboxEvent, true);
        session.delete(outboxEvent);
    }
    
    private OutboxEvent getDataFromEvent(DomainEvent event) {
        return new OutboxEvent(
            event.getAggregateType(),
            event.getAggregateId(),
            event.getType(),
            event.getTimestamp(),
            event.getPayload()
        );
    }
}
