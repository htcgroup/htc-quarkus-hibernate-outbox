package com.gitlab.htcgroup.outbox;

import com.fasterxml.jackson.databind.JsonNode;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.UUID;

@Entity(name = "outboxevent")
public class OutboxEvent {
    
    @Id
    @GeneratedValue
    @Type(type = "org.hibernate.type.UUIDCharType")
    @Column(columnDefinition = "CHAR(36)")
    private UUID id;
    
    @Column(name = "aggregate_type")
    @NotNull
    private String aggregateType;
    
    @Column(name = "aggregate_id")
    @NotNull
    private String aggregateId;
    
    @Column(name = "event_type")
    @NotNull
    private String type;
    
    @NotNull
    private Instant timestamp;
    
    @Convert(converter = JsonNodeAttributeConverter.class)
    @Column(columnDefinition = "JSON")
    @NotNull
    private JsonNode payload;
    
    protected OutboxEvent() {
    }
    
    public OutboxEvent(String aggregateType, String aggregateId, String type, Instant timestamp, JsonNode payload) {
        this.aggregateType = aggregateType;
        this.aggregateId = aggregateId;
        this.type = type;
        this.timestamp = timestamp;
        this.payload = payload;
    }
    
    public UUID getId() {
        return id;
    }
    
    public void setId(UUID id) {
        this.id = id;
    }
    
    public String getAggregateType() {
        return aggregateType;
    }
    
    public void setAggregateType(String aggregateType) {
        this.aggregateType = aggregateType;
    }
    
    public String getAggregateId() {
        return aggregateId;
    }
    
    public void setAggregateId(String aggregateId) {
        this.aggregateId = aggregateId;
    }
    
    public String getType() {
        return type;
    }
    
    public void setType(String type) {
        this.type = type;
    }
    
    public Instant getTimestamp() {
        return timestamp;
    }
    
    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }
    
    public JsonNode getPayload() {
        return payload;
    }
    
    public void setPayload(JsonNode payload) {
        this.payload = payload;
    }
}
